// Create a server in express.js
const express = require('express');
const app = express(); // This is already equivalent to http.createServer
const mongoose = require('mongoose');
const port = 4000;

// Connecting to MongoDB
mongoose.connect("mongodb+srv://AmirCastaneda:admindb@b145-zuitt-cluster.mjhg7.mongodb.net/session32?retryWrites=true&w=majority", {
  useUnifiedTopology: true,
  useNewUrlParser: true
});
// admindb

// db.on("error", console.error.bind(console, "There is an error with the connection"));
// db.once("open", () => console.log("Successfully connected to the database"));

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// Mongoose Schema == blueprint of data
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: 'pending'
	}
});

// Model
const Task = mongoose.model("Task", taskSchema);

// Business Logic
app.post('/tasks', (req, res)=> {
	Task.findOne({name: req.body.name}, (err, result) => {
		if(result != null && result.name === req.body.name) {
			return res.send(`Duplicate task found`)
		}
		else {
			let newTask = new Task({
				name: req.body.name
			});
		newTask.save((saveErr, savedTask) => {
			if(saveErr) {
				return console.error(saveErr);
			}
			else {
				return res.status(200).send(`New task created`);
			}
		})
		}
	})
});

// Retrieving all tasks
app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if(err) {
			return console.log(err);
		}
		else {
			return res.status(200).json({tasks: result})
		}
	})
})

// Updating task name
// : - for URL parameter
app.put("/tasks/update/:taskId", (req, res) => {
	// params refers to the :taskId
	let taskId = req.params.taskId;
	let name = req.body.name;

	Task.findByIdAndUpdate(taskId, {name: name}, (err, updatedTask)  => {
		if(err) {
			console.log(err);
		}
		else {
			res.send(`Congratulations the task has been updated`);
		}
	})
})

// Delete Task
app.delete("/tasks/archive-task/:taskId", (req, res) => {
	let taskId = req.params.taskId;
	Task.findByIdAndDelete(taskId, (err, deletedTask) => {
		if(err) {
			console.log(err);
		}
		else {
			res.send(`${deletedTask} has been deleted`)
		}
	})
})


app.listen(port, () => console.log(`Server running at port ${port}`));