// Create a server in express.js
const express = require('express');
const mongoose = require('mongoose');
const app = express();
const port = 4000;

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Connecting to MongoDB
mongoose.connect("mongodb+srv://AmirCastaneda:admindb@b145-zuitt-cluster.mjhg7.mongodb.net/session32-Activity?retryWrites=true&w=majority", {
	useUnifiedTopology : true,
	useNewUrlParser : true
});

const userSchema = new mongoose.Schema({
	email: String,
	userName: String,
	password: String,
	age: Number,
	isAdmin: Boolean
});

const User = mongoose.model("User", userSchema);

// Business Logic
app.post('/users/signup', (req, res) => {
	User.findOne({email: req.body.email}, (err, result) => {
		if(result != null && result.email === req.body.email) {
			res.send(`Duplicate`);
		}
		else {
			let newUser = new User({
				email: req.body.email,
				userName: req.body.userName,
				password: req.body.password,
				age: req.body.age
			});
		newUser.save((saveErr, savedUser) => {
			if(saveErr) {
				return console.error(saveErr);
			}
			else {
				return res.status(200).send(`New user created: ${newUser}`);
			}
		})
		}
	})
});

app.get("/users", (req, res) =>  {
	User.find({}, (err, result) => {
		if(err) {
			return console.log(err);
		}
		else {
			return res.status(200).json({users: result});
		}
	})
});

app.put('/users/update-user/:userId', (req, res) => {
	let userId = req.params.userId;
	let newUserName = req.body.userName;

	User.findByIdAndUpdate(userId, {userName: newUserName}, (err, updatedUserName) => {
		if(err) {
			console.log(err);
		}
		else {
			res.send(`Congratulations, the new username is ${newUserName}`);
		}
	})
});

app.delete('/users/archive-user/:userId', (req, res) => {
	let userId = req.params.userId;
	User.findByIdAndDelete(userId, (err, deletedUser) => {
		if(err) {
			console.log(err);
		}
		else {
			res.send(`${deletedUser} has been deleted`);
		}
	})
})


app.listen(port, ()=> console.log(`Server running at port ${port}`));